# VoIDmetricsGenerator

Simple tool for generating the [VoID](https://www.w3.org/TR/void/) metrics of a dataset


# Todo

- [ ] make sure all the SPARQL queries handle named graphs
- [ ] generate a VoID template
- [ ] support
    - [ ] local file
    - [ ] endpoint
        - [ ] make sure that the triplestore-related triples and graphs are ignored
- [ ] use a `CONSTRUCT` query to generate the core of the VoID description
